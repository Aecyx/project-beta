from django.db import models
from django.core.validators import MinLengthValidator


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True, validators=[MinLengthValidator(17, 'this field must contain at least 17 characters')])

class Technician(models.Model):
    name = models.CharField(max_length=200, null=True)
    employee_id = models.PositiveSmallIntegerField(unique=True)


class ServiceAppointment(models.Model):
    new_vin = models.CharField(max_length=17, unique=True, validators=[MinLengthValidator(17, 'this field must contain at least 17 characters')])
    name = models.CharField(max_length=200)
    date = models.DateField(null=True, blank=True)
    time = models.TimeField(null=True, blank=True)
    reason = models.TextField(default=False)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.PROTECT,
        null=True
    )
    vip = models.BooleanField(default=False)
    finished = models.BooleanField(default=False)
