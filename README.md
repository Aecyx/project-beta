# CarCar

Team:

* Margaret Ketchum - Automobile Service
* Matt Sabo - Sales

## Design

## Service microservice

My models in the Services microservice are the AutomobileVO, Technician, and ServiceAppointment.

The AutomobileVO model is a value object that is used for our poller. It polls the vin from the Automobile model in Inventory microservice. I set my poller to poll every 5 seconds that vin information from Inventory to AutomobileVO. This allowed me to later compare vins from the Inventory microservice to the vins(new_vin) I was entering in my Services microservice. This was so useful for when I needed to find out if a customer is VIP customer or not.

The Technician model has name(which is a CharField) and employee_id(which is a PositiveSmallIntegerField). I am able to use the information from Technician to attach to new vins I enter or existing vins from the Inventory microservice to attach to my service appointments.

The ServiceApopintment model has new_vin, name, date, time, reason, technician(a ForeignKey linked to the Technician model), vip, and finished. All of this information helps me make my Service Appointments forms and list their information. This model interacts with the Inventory microservice by comparing the vins in the existing database to the new_vins that we enter into the system to check if the vin is existing in our system already. This allows us to know if the customer is a vip or not and if they get the special vip treatment.

## Sales microservice

we created an AutomobileVO that polls vins from the Automobile model. We use these vins in our salesrecords so we can keep track of the automobiles that used to be in our inventory. Other models such as SalesPerson and PotentialCustomer are simple models each with a unique identifier that represent people like employees or customers. These models have instances that are required for our SalesRecord model. This model has 3 foreignkey relationships. Two linking to the PotentialCustomer and SalesPerson models, and the third being the AutomobileVO where we grab the vehicle's VIN number included in the sale. I have a PROTECT relationship between these models on_delete, so that we keep our sales records safe in case anyone tries deleting an employee/customer/vin that was involved in a sale.
